package com.systemTest;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.teste.Core;


public class LoginWordpressSystemTesting {

	public WebDriver webDriver;
	 
	
	//abrir o endereco informado para que possa ser iniciado os testes
	@Before
	public void openUrl() {
		System.setProperty("webdriver.gecko.driver", "C:\\gecko\\geckodriver.exe"); //informa onde está localizado o geckodriver
		webDriver=new FirefoxDriver();//instancia da classe responsável para abrir o navegador
		webDriver.get("https://br.wordpress.com/");//abertura do endereco desejado
		
	}
	
	//navega para a tela de cadastro inicial do Wordpress
	@Test
	public void irParaLogin() {
		//utilizando a Class Core
		//Core core = new Core(webDriver);
		//core.clickByClass("login-link");
		
		//utilizando Seleniun
		webDriver.findElement(By.className("login-link")).click();//busca o elemento "id" e clica sobre o mesmo
		preencherDadosLogin();
	}
	
	//preenchimento do formulario de login
	public void preencherDadosLogin() {
		//utilizando a Class Core
		Core core = new Core(webDriver);
		core.inputById("usernameOrEmail", "teste@gmail.com");//trocar pelo seu usuario
		core.inputById("password", "meupassword123");//trocar pela senha do usuário que esta informando
		core.clickByClass("is-primary");	
		
		//utilizando o Selenium
//		webDriver.findElement(By.id("usernameOrEmail")).sendKeys("email@email.com");
//		webDriver.findElement(By.id("usernameOrEmail")).sendKeys("senhadousuario");
//		webDriver.findElement(By.className("is-primary")).click();
		
	}
}
